const { response, request } = 'express';
const DataPerson = require('../data/dataJson')
const Persona = require('../models/persona')

const dataPeronas = new DataPerson()

//Get
const personaGet = (req, res = response) => {
  let personas = dataPeronas.getDB()

  res.json({
    msg: 'Datos optenidos con exito',
    personas
  });
};

//Put
const personaPut = (req = request, res = response) => {
  const { id } = req.params;

  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  
  const index = dataPeronas.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPeronas.editPersona({ nombres, apellidos, ci, direccion, sexo }, index)
    res.json({
      msg: `Persona con el id: ${id} se modifico`
    });
  }else {
    res.json({
      msg: `Persona con el id: ${id} no se encontro`
    });
  }

};

//Post
const personaPost = (req = request, res) => {
  const { nombres, apellidos, ci, direccion, sexo } = req.body;
  nuevaPersona = new Persona(nombres, apellidos, ci, direccion, sexo)
  dataPeronas.addPersona(nuevaPersona)

  res.status(201).json({
    msg: 'Persona Agregada con exito'
  });
}

//Deleted
const personaDelete = (req, res = response) => {
  
  const { id } = req.params;

  const index = dataPeronas.data.findIndex(object => object.id === id)

  if (index !== -1) {
    dataPeronas.deletPersona(index)
    res.json({
      msg: `Persona con el id: ${id} se elimino`
    });
  } else {
    res.json({
      msg: 'Persona no encontrada'
    }); 
  }
  
};

module.exports = {
  personaPost,
  personaGet,
  personaPut,
  personaDelete
};
