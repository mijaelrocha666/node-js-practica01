const { Router } = require('express');
const { 
  personaPost,
  personaGet,
  personaPut,
  personaDelete
 } = require('../controller/persona');

const router = Router();

router.post('/', personaPost);
router.get('/', personaGet);
router.put('/:id', personaPut);
router.delete('/:id', personaDelete);

module.exports = router;
